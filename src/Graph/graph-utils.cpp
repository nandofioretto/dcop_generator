#include <queue>
#include <utils.hpp>

#include "Graph/graph.hpp"
#include "Graph/graph-utils.hpp"

using namespace dcop_generator;
using namespace std;
using namespace misc_utils;


bool graph_utils::connected(const graph& pGraph) {

	if (pGraph.get_nb_edges() < (pGraph.get_nb_nodes() - 1))
		return false;

	enum node_state {
		kUnvisited = 0, kTraversed, kOpened, kClosed
	};

	vector<node_state> status(pGraph.get_nb_nodes(), kUnvisited);
	queue<int> traversal_queue;
	int n_components = 0;

	for (int n : pGraph.get_nodes()) {
		if (status[n] == kTraversed) continue;

		traversal_queue.push(n);

		while (!traversal_queue.empty()) {
			int t = traversal_queue.front();
			traversal_queue.pop();

			for (int u : pGraph.get_neighbors(t)) {
				if (status[u] == kUnvisited) {
					status[u] = kOpened;
					traversal_queue.push(u);
				}
			}
			status[t] = kTraversed;
		}
		++n_components;
	}
	return (n_components == 1);
}


vector< vector<int> > graph_utils::cliques(const graph &pGraph, int size) {
	vector<int> nodes = pGraph.get_nodes();
	vector<int> clique(size);
	vector<vector<int> > cliques;
	do {
		bool is_clique = true;
		for (int u = 0; u < size; ++u) {
			for (int v = u + 1; v < size; ++v) {
				if (!pGraph.get_edge(nodes[u], nodes[v])) {
					is_clique = false;
					break;
				}
			}
			if (!is_clique) break;
		}

		if (is_clique) {
			clique.assign(nodes.begin(), nodes.begin() + size);
			cliques.push_back(clique);
		}

	} while (utils::next_combination
			(nodes.begin(), nodes.begin() + size, nodes.end()));

	return cliques;
}