#include <fstream>
#include <sstream>
#include <vector>
#include <string>

#include "IO/output-xml.hpp"
#include "Kernel/agent.hpp"
#include "Kernel/domain.hpp"
#include "Kernel/variable.hpp"
#include "Kernel/relation.hpp"
#include "Kernel/constraint.hpp"


using namespace dcop_generator;
using namespace misc_utils;
using namespace std;

output_xml::output_xml(std::string pathout, std::string fileout, int nb_instances, int nb_start)
    : output(pathout, fileout, nb_instances, nb_start)
{}


void output_xml::to_string(instance::ptr instance) {
    output::to_string(instance);
}


void output_xml::to_string(instance::ptr instance, std::string file) {

  size_t found = file.find_last_of(".");
  std::string extension = file.substr(found+1);
  if (found == std::string::npos)
    file += ".xml";
  else if(extension.compare("xml") != 0){
    file = file.substr(0, found);
    file += ".xml";
  }
  
  std::cout << "file is: " << file << std::endl; 
  
  std::ofstream os ( file.c_str() );
  if( os.is_open() ) {
      dump_instance(os);
      dump_presentation(os, instance->to_string(), instance->get_max_constraint_arity());
      dump_agents(os, instance->get_agents());
      dump_domains(os, instance->get_domains());
      dump_variables(os, instance->get_variables());
      dump_relations(os, instance->get_relations());
      dump_constraints(os, instance->get_constraints());
      os << "</instance>" << std::endl;
      os.close();
  }
  else std::cout << "Cannot open file: " << file << std::endl;
  
}


void output_xml::dump_instance(std::ostream &os) {

    os << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    os << open_xml_tag << "instance"
    << " xmlns:xsi="
    << txt_d << "http://www.w3.org/2001/XMLSchema-instance" << txt_d
    << " xsi:noNamespaceSchemaLocation="
    << txt_d << "src/ch/epfl/lia/frodo/algorithms/XCSPschema.xsd" << txt_d
    << ">" << std::endl;
}


void output_xml::dump_presentation(std::ostream &os, string name, int max_con_arity) {
    os << open_xml_tag << "presentation"
    << " name=" << txt_d << name << txt_d
    << " maxConstraintArity=" << txt_d << max_con_arity << txt_d
    << " maximize=" << txt_d << "true" << txt_d
    << " format=" << txt_d << "XCSP 2.1_FRODO" << txt_d
    << close_xml_tag;
}


void output_xml::dump_agents(std::ostream &os, std::vector<agent::ptr> agents) {

  os << "<agents" << " nbAgents=" << txt_d << agents.size() 
     << txt_d << ">" << std::endl;
  for (agent::ptr agent : agents)
  {
    os << "\t" << open_xml_tag << "agent"
       << " name=" << txt_d << agent->get_name() << txt_d
       << close_xml_tag;
  }
  os << "</agents>" << std::endl;
}


void output_xml::dump_domains(std::ostream &os, std::vector<domain::ptr> domains) {

  os << "<domains" << " nbDomains=" << txt_d << domains.size() 
     << txt_d << ">" << std::endl;
  for (domain::ptr dom : domains)
  {
    os << "\t" << open_xml_tag << "domain"
       << " name=" << txt_d << dom->get_name() << txt_d
       << " nbValues=" << txt_d << dom->get_size() << txt_d << ">"
       << dom->get_min() << ".." << dom->get_max()
       << "</domain>" << std::endl;
  }
  os << "</domains>" << std::endl;
}


void output_xml::dump_variables(std::ostream &os, std::vector<variable::ptr> variables) {

  os << "<variables" << " nbVariables=" << txt_d << variables.size() 
     << txt_d << ">" << std::endl;
  for (variable::ptr var : variables)
  {
    os << "\t" << open_xml_tag << "variable"
       << " name=" << txt_d << var->get_name() << txt_d
       << " domain=" << txt_d << var->get_domain() << txt_d
       << " agent=" << txt_d << var->get_agent() << txt_d
       << close_xml_tag;
  }
  os << "</variables>" << std::endl;
}


void output_xml::dump_relations( std::ostream &os, std::vector<relation::ptr> relations) {

  os << "<relations" << " nbRelations=" << txt_d << relations.size() 
     << txt_d  << ">"  << std::endl;
  for (relation::ptr rel : relations)
  {
    os << "\t" << open_xml_tag << "relation"
       << " name=" << txt_d << rel->get_name() << txt_d
       << " arity=" << txt_d << rel->get_arity() << txt_d
       << " nbTuples=" << txt_d << rel->get_nb_tuples() << txt_d
       << " semantics=" << txt_d << rel->get_semantics() << txt_d
       << " defaultCost=" << txt_d << output::cost_to_string(rel->get_default_cost()) << txt_d
       << ">";
    int i=0;
    for(relation::relentry_t& entry : rel->get_tuples())
    {
      os << output::cost_to_string(entry.second) << ":";
      for(int value : entry.first) // scan each value in the entry tuple 
        os << value << " ";
      if (++i < rel->get_nb_tuples()) os << "|";
    }
    os << "</relation>" << std::endl;
  }
  os << "</relations>" << std::endl;
}


void output_xml::dump_constraints(std::ostream &os, std::vector<constraint::ptr> constraints) {

  os << "<constraints" << " nbConstraints=" << txt_d << constraints.size() 
     << txt_d << ">" << std::endl;
  for (constraint::ptr con : constraints)
  {
    os << "\t" << open_xml_tag << "constraint"
       << " name=" << txt_d << con->get_name() << txt_d
       << " arity=" << txt_d << con->get_arity() << txt_d
       << " scope=" << txt_d; 
    int i=0;
    for (const std::string& var : con->get_scope())
    {
      os << var;
      if (++i < con->get_arity()) os << " ";
    }
    os << txt_d;
    os << " reference=" << txt_d << con->get_relation() << txt_d
       << close_xml_tag;
  }
  os << "</constraints>" << std::endl;
}
